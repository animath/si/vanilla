FROM php:7.4-apache

RUN set -xe; \
    apt-get update \
    && apt-get install -y \
        # for intl extension
        libicu-dev \
        # for gd
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        # for mbstring
        libonig-dev \
        # memchached extension
        libmemcached-dev \
        # allow mailing to work
        sendmail \
        # allow reading of image exif
        exiftool \
	zip \
	unzip \
	libzip-dev \
	npm \
	git \
    && npm install -g yarn \
    # pecl installs
    && docker-php-ext-install exif \
    && pecl install xdebug \
    && pecl install memcached \
    # enable pecl installed extentions
    && docker-php-ext-enable xdebug \
    && docker-php-ext-enable memcached \
    && docker-php-ext-enable exif \
    # built in extensions install
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) \
        gd \
        mbstring \
        pdo \
        pdo_mysql \
        intl \
	zip \
    # cleanup
    && pecl clear-cache \
    && rm -rf \
        /var/lib/apt/lists/* \
        /usr/src/php/ext/* \
        /tmp/*

COPY . /var/www/html

RUN chown -R www-data:www-data /var/www/html

WORKDIR /var/www/html

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
	php composer-setup.php --1 && \
	php -r "unlink('composer-setup.php');" && \
	php composer.phar install && \
	yarn up

RUN a2enmod rewrite && mv .htaccess.dist .htaccess
